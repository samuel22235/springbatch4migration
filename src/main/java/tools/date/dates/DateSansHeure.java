package tools.date.dates;


import org.joda.time.LocalDateTime;


public class DateSansHeure extends Date {

	public java.util.Date getDate() {
		return dateLocale == null ? null : dateLocale.toDateTime().toDate();
	}

	public void setDate(java.util.Date date) {
		dateLocale = new LocalDateTime(date);
	}

	public DateSansHeure() {
		super();
	}

	public DateSansHeure(Date date, DateCategorie categorie) {
		super(date, categorie);
	}

	public DateSansHeure(java.util.Date date, DateCategorie categorie) {
		super(date, categorie);
	}

	public DateSansHeure(DateCategorie categorie) {
		super(categorie);
	}

	public DateSansHeure(LocalDateTime dateLocale, DateCategorie categorie) {
		super(dateLocale, categorie);
	}

	public DateSansHeure(String date, DateCategorie categorie) {
		super(date, categorie);
	}

	public static <T extends DateSansHeure> java.util.Date toDate(T uneDate) {
		return uneDate != null ? uneDate.getDate() : null;
	}

	/**
	 * Renvoit une string représentant la date au format américain 'YYYY-MM-DD'
	 * qui est le format par défaut de Postgres
	 * 
	 * @return
	 */
	public String getStringFromDatePatternAmericain() {
		return toStringFromPattern(DateConstantes.FORMAT_DATE_SANS_HEURE_POSTGRESQL);
	}
}
