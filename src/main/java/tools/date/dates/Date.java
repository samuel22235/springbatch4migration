package tools.date.dates;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Locale;

import org.joda.time.Days;
import org.joda.time.Instant;
import org.joda.time.LocalDateTime;
import org.joda.time.Months;
import org.joda.time.Seconds;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Classe Date générique représentant une date avec une catégorie (ANNEE, MOIS,
 * JOUR, JOUR_AVEC_HEURE). Utilise la librairie joda-time. On retrouve les mêmes
 * méthodes que dans joda-time. Est prévue pour stocker la date en base sous
 * format chaîne de caractères et selon le format fixé pour chaque catégorie de
 * date (@see DateCategorie).
 */
public class Date implements Comparable<Date> {

	// Date locale interne
	protected LocalDateTime dateLocale;

	// Catégorie de date
	protected DateCategorie categorie;

	// Représentation de la date sous forme de chaîne
	protected String dateAsString;

	/*
	 * -------------------------------------------------------------------------
	 * CONSTRUCTEURS Les constructeurs sont volontairement protected pour
	 * inciter l'utilisation des sous-classes, plutôt que de cette classe.
	 * -------------------------------------------------------------------------
	 */

	protected Date() {
		this(new LocalDateTime(), DateCategorie.JOUR_AVEC_HEURE);
	}

	protected Date(DateCategorie categorie) {
		this(new LocalDateTime(), categorie);
	}

	protected Date(Date date, DateCategorie categorie) {
		this(date.dateLocale, categorie);
	}

	protected Date(java.util.Date date, DateCategorie categorie) {
		this(new LocalDateTime(date), categorie);
	}

	protected Date(LocalDateTime dateLocale, DateCategorie categorie) {
		setLocalDate(dateLocale, categorie);
		this.categorie = categorie;
	}

	protected Date(String date, DateCategorie categorie) {
		this.categorie = categorie;
		setDateOrFail(date);
	}


	/**
	 * Récupérer la date sous forme de chaîne au bon format. Si la date locale
	 * interne stockant la date est nulle, renvoit null plutôt qu'une chaîne
	 * vide.
	 */
	public String getDateString() {
		if (dateAsString == null && dateLocale != null) {
			dateAsString = dateLocale.toString(categorie.getDateTimeFormatter());
		}
		return dateAsString;
	}

	public void setDateString(String uneDate) {
		setDateOrFail(uneDate);
	}

	// Version privée de la méthode de mise à jour de la date avec contrôles
	private void setDateOrFail(String uneDate) {
		try {
			dateLocale = LocalDateTime.parse(uneDate, categorie.getDateTimeFormatter());
			dateAsString = uneDate;
		} catch (IllegalArgumentException e) {
			System.out.println("ERREUR");
		}
	}

	protected void setLocalDate(LocalDateTime localDateTime, DateCategorie dateCategorie) {
		switch (dateCategorie) {
		case ANNEE:
			localDateTime = localDateTime.withMonthOfYear(1);
		case MOIS:
			localDateTime = localDateTime.withDayOfMonth(1);
		case JOUR:
			localDateTime = localDateTime.withTime(0, 0, 0, 0);
			break;
		default:
			break;
		}
		dateLocale = localDateTime;
	}

	/*
	 * -------------------------------------------------------------------------
	 * AUTRES METHODES
	 * -------------------------------------------------------------------------
	 */

	/**
	 * Retourne la date sous forme de chaîne au bon format.
	 */
	public String toString() {
		// return "{" + categorie.name() + "} [" +getDate()+", "+dateLocale+"]";
		return getDateString();
	}

	/**
	 * Retourne la date sous forme de chaîne au format par défaut de la
	 * catégorie de date.
	 */
	public String toString(DateCategorie dateCategorie) {
		return dateLocale != null ? dateLocale.toString(categorie.getDateTimeFormatter()) : null;
	}

	/**
	 * Transforme une date en string selon le pattern souhaité
	 */
	public String toString(Pattern pattern) {
		return toStringFromPattern(pattern.getMotif());
	}

	/**
	 * Transforme une date en string selon le pattern souhaité
	 */
	public String toStringFromPattern(String pattern) {
		String dateAuBonFormat = null;
		try {
			dateAuBonFormat = dateLocale.toString(pattern, Locale.FRANCE);
		} catch (IllegalArgumentException e) {
			System.out.println("ERREUR");
		}
		return dateAuBonFormat;
	}

	public Long toMillisecond() {
		return new Instant(dateLocale.toDate()).getMillis();
	}

	/**
	 * Implémentation de compareTo. On ne tient pas compte de la catégorie de la
	 * date dans ce test. Autrement dit, l'année 2013, le mois 201301 et le jour
	 * 20130101 sont identiques selon cette comparaison.
	 */
	@Override
	public int compareTo(Date uneAutreDate) {
		return dateLocale.compareTo(uneAutreDate.dateLocale);
	}

	/**
	 * Tester si une date donnée est postérieure à la date manipulée.
	 */
	public boolean isAfter(Date uneAutreDate) {
		return dateLocale.isAfter(uneAutreDate.dateLocale);
	}

	/**
	 * Tester si une date donnée est postérieure ou égale à la date manipulée.
	 */
	public boolean isAfterOrEqual(Date uneAutreDate) {
		return dateLocale.isAfter(uneAutreDate.dateLocale)
				|| dateLocale.isEqual(uneAutreDate.dateLocale);
	}

	/**
	 * Tester si une date donnée est antérieure à la date manipulée.
	 */
	public boolean isBefore(Date uneAutreDate) {
		return dateLocale.isBefore(uneAutreDate.dateLocale);
	}

	/**
	 * Tester si une date donnée est antérieure ou égale à la date manipulée.
	 */
	public boolean isBeforeOrEqual(Date uneAutreDate) {
		return dateLocale.isBefore(uneAutreDate.dateLocale)
				|| dateLocale.isEqual(uneAutreDate.dateLocale);
	}

	/**
	 * Tester si une date donnée est égale à la date manipulée (au sens
	 * chronologique). Ne pas confondre avec la méthode equals qui teste si deux
	 * instances sont identiques dans un sens plus technique (ie même date ET
	 * même catégorie).
	 */
	public boolean isEqual(Date uneAutreDate) {
		return dateLocale.isEqual(uneAutreDate.dateLocale);
	}

	/**
	 * Récupérer le jour de la semaine correspondant à la date manipulée.
	 * 
	 * @see DateConstantes
	 */
	public int getDayOfWeek() {
		return dateLocale.getDayOfWeek();
	}

	/**
	 * Récupérer le jour de la semaine correspondant à la date manipulée.
	 * 
	 * @see DateConstantes
	 */
	public int getDayOfMonth() {
		return dateLocale.getDayOfMonth();
	}

	/**
	 * Récupérer le mois de l'année correspondant à la date manipulée.
	 * 
	 * @see DateConstantes
	 */
	public int getMonthOfYear() {
		return dateLocale.getMonthOfYear();
	}

	/**
	 * Récupérer l'année correspondant à la date manipulée.
	 */
	public int getYear() {
		return dateLocale.getYear();
	}

	/**
	 * Récupérer l'heure correspondant à la date manipulée.
	 */
	public int getHour() {
		return dateLocale.getHourOfDay();
	}

	/**
	 * Récupérer les minutes de la date correspondant à la date manipulée.
	 */
	public int getMinuteOfHour() {
		return dateLocale.getMinuteOfHour();
	}

	/**
	 * Récupérer les secondes de la date correspondant à la date manipulée.
	 */
	public int getSecondOfMinute() {
		return dateLocale.getSecondOfMinute();
	}

	/**
	 * Incrémenter une date
	 */
	public Date plus(int units) {
		Date result = null;
		switch (categorie) {
		case ANNEE:
			result = new Date(dateLocale.plusYears(units), DateCategorie.ANNEE);
			break;
		case MOIS:
			result = new Date(dateLocale.plusMonths(units), DateCategorie.MOIS);
			break;
		case JOUR:
			result = new Date(dateLocale.plusDays(units), DateCategorie.JOUR);
			break;
		case JOUR_AVEC_HEURE:
			result = new Date(dateLocale.plusSeconds(units),
					DateCategorie.JOUR_AVEC_HEURE);
			break;
		}
		return result;
	}

	/**
	 * Réécriture de la méthode hashcode pour cohérence avec la nouvelle méthode
	 * equals.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((categorie == null) ? 0 : categorie.hashCode());
		result = prime * result
				+ ((dateLocale == null) ? 0 : dateLocale.hashCode());
		return result;
	}

	/**
	 * Réécriture de la méthode equals. On considère que deux dates sont égales
	 * si à la fois, les dates locales internes et les catégories de dates sont
	 * égales.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Date other = (Date) obj;
		if (categorie != other.categorie)
			return false;
		if (dateLocale == null) {
			if (other.dateLocale != null)
				return false;
		} else if (!dateLocale.equals(other.dateLocale))
			return false;
		return true;
	}

	/*
	 * -------------------------------------------------------------------------
	 * METHODES STATIQUES UTILITAIRES
	 * -------------------------------------------------------------------------
	 */

	/**
	 * Tranformer une chaîne en instance de l'objet Date. La méthode teste le
	 * format donné en paramètre. Si le format convient, une date est créée. Si
	 * la chaîne ne représente pas une valeur conforme, la valeur nulle est
	 * retournée.
	 */
	public static Date stringToDate(String uneDate, DateCategorie categorie, Pattern pattern) {
		return stringToDate(uneDate, categorie, pattern.getMotif());
	}
	
	/**
	 * Tranformer une chaîne en instance de l'objet Date. La méthode teste le
	 * format donné en paramètre. Si le format convient, une date est créée. Si
	 * la chaîne ne représente pas une valeur conforme, la valeur nulle est
	 * retournée.
	 */
	public static Date stringToDate(String uneDate, DateCategorie categorie, String pattern) {
		Date dateResultat = null;
		try {
			DateTimeFormatter datetimeformatter = DateTimeFormat.forPattern(pattern).withLocale(Locale.FRANCE);
			LocalDateTime localDateTime = LocalDateTime.parse(uneDate, datetimeformatter);
			dateResultat = new Date(localDateTime, categorie);
		} catch (Exception e) {
			dateResultat = null;
		}
		return dateResultat;
	}

	/**
	 * Tranformer une chaîne en instance de l'objet Date. La méthode teste le
	 * format correspondant à la catégorie précisée. Si le format convient, une
	 * date est créée. Si la chaîne ne représente pas une valeur conforme, la
	 * valeur nulle est retournée.
	 */
	public static Date stringToDate(String uneDate, DateCategorie categorie) {
		Date dateResultat = null;
		try {
			dateResultat = new Date(uneDate, categorie);
		} catch (Exception e) {
			dateResultat = null;
		}
		return dateResultat;
	}

	/**
	 * Tranformer une chaîne en instance de l'objet Date. La méthode teste
	 * chaque format un par un dans l'ordre défini dans l'énumération
	 * DateCategorie. Dès qu'un format convient, une date est créée. Si aucun
	 * format ne convient, la valeur null est retournée.
	 */
	public static Date stringToDate(String uneDate) {
		Date dateResultat = null;
		if (uneDate != null && !uneDate.trim().isEmpty()) {
			// On essaye avec les différents formats un par un selon l'ordre fixé dans l'enumération
			LinkedList<DateCategorie> categories = new LinkedList<DateCategorie>(Arrays.asList(DateCategorie.values()));
			while (!categories.isEmpty() && dateResultat == null) {
				dateResultat = stringToDate(uneDate, categories.removeFirst());
			}
		}
		return dateResultat;
	}

	/**
	 * Tester si une chaîne est une réprésentation valide d'une date de la
	 * catégorie indiquée.
	 */
	public static boolean isValid(String uneDate, DateCategorie uneCategorie) {
		return stringToDate(uneDate, uneCategorie) != null;
	}

	/**
	 * Tester si une chaîne est une réprésentation valide d'une date.
	 */
	public static boolean isValid(String uneDate) {
		return stringToDate(uneDate) != null;
	}

	/**
	 * Calculer l'écart entre deux dates, soit en année, en mois, en jour ou en
	 * secondes.
	 */
	public static <T extends Date> int between(T debut, T fin,
			DateCategorie categorieEcart) {
		int between = 0;
		switch (categorieEcart) {
		case ANNEE:
			between = Years.yearsBetween(debut.dateLocale, fin.dateLocale)
					.getYears();
			break;
		case MOIS:
			between = Months.monthsBetween(debut.dateLocale, fin.dateLocale)
					.getMonths();
			break;
		case JOUR:
			between = Days.daysBetween(debut.dateLocale, fin.dateLocale)
					.getDays();
			break;
		case JOUR_AVEC_HEURE:
			between = Seconds.secondsBetween(debut.dateLocale, fin.dateLocale)
					.getSeconds();
			break;
		}
		return between;
	}

	/**
	 * Comparer 2 dates en forçant la catégorie de comparaison. Par exemple, la
	 * première date peut-être un jour et la seconde une année, mais on veut les
	 * comparer en tant que mois. La procédure va d'abord transformer le jour en
	 * mois en tronquer le jour du mois et va ensuite tarnsformer l'année en
	 * mois en se plaçant en janvier par défaut. Ensuite, on calcule le nombre
	 * de mois séparant les deux dates ainsi obtenues.
	 * 
	 * @param oneDate
	 * @param otherDate
	 * @param categorieComparaison
	 * @return
	 */
	public static <T extends Date, U extends Date> int extendedCompare(T oneDate, U otherDate, DateCategorie categorieComparaison) {
		Date oneDateEnCategorieComparaison = new Date(oneDate, categorieComparaison);
		Date otherDateEnCategorieComparaison = new Date(otherDate, categorieComparaison);
		return between(oneDateEnCategorieComparaison, otherDateEnCategorieComparaison, categorieComparaison);
	}

	/**
	 * Minimum de deux dates avec la règle qu'une date nulle représente
	 * "le début des temps" ie : a==null ou b==null => min(a, b) = null
	 */
	public static <T extends Date> T min(T a, T b) {
		T min = null;
		if (a != null && b != null) {
			min = a.compareTo(b) > 0 ? b : a;
		}
		return min;
	}

	/**
	 * Minimum de n dates avec la règle qu'une date nulle représente
	 * "le début des temps" ie : a==null ou b==null => min(a, b) = null.
	 * Si aucune date, renvoie null.
	 * Si une seule date, renvoie cette date unique.
	 */
	@SafeVarargs
	public static <T extends Date> T min(T... a) {
		T resultat = null;
		if (a != null && a.length > 0) {
			resultat = a[0];
			for (int i = 1; i < a.length; i++) {
				resultat = min(resultat, a[i]); 
			}
		}
		return resultat;
	}

	/**
	 * Maximum de deux dates avec la règle qu'une date nulle représente
	 * "la fin des temps" ie : a==null ou b==null => max(a, b) = null
	 */
	public static <T extends Date> T max(T a, T b) {
		T max = null;
		if (a != null && b != null) {
			max = a.compareTo(b) > 0 ? a : b;
		}
		return max;
	}

	/**
	 * Maximum de n dates avec la règle qu'une date nulle représente
	 * "la fin des temps" ie : a==null ou b==null => min(a, b) = null.
	 * Si aucune date, renvoie null.
	 * Si une seule date, renvoie cette date unique.
	 */
	@SafeVarargs
	public static <T extends Date> T max(T... a) {
		T resultat = null;
		if (a != null && a.length > 0) {
			resultat = a[0];
			for (int i = 1; i < a.length; i++) {
				resultat = max(resultat, a[i]); 
			}
		}
		return resultat;
	}

	/**
	 * Transformation d'une date en chaîne.
	 * Renvoit null si date nulle.
	 */
	public static <T extends Date> String toString(T a, String pattern) {
		return a != null ? a.toStringFromPattern(pattern) : null;
	}

	/**
	 * Méthode qui transforme une date sous forme de chaîne : - du format mmaaaa
	 * vers le format aaaamm - du format jjmmaaaa vers le format aaaammjj
	 * 
	 * @param date
	 * @return date au format inversé
	 */
	public static String reverseDate(String date) {
		StringBuilder dateAaaammjj = new StringBuilder();

		if (date.length() == 8) {
			dateAaaammjj.append(date.substring(4, 8));
			dateAaaammjj.append(date.substring(2, 4));
			dateAaaammjj.append(date.substring(0, 2));
		} else {
			dateAaaammjj.append(date.substring(2, 6));
			dateAaaammjj.append(date.substring(0, 2));
		}

		return dateAaaammjj.toString();
	}

}
