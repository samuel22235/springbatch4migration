package tools.date.dates;


import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.joda.time.LocalDateTime;


public class Jour
    extends DateSansHeure
{
    
    public Jour() {
        super(new LocalDateTime(), DateCategorie.JOUR);
    }


    public Jour(String jour) {
        super(jour, DateCategorie.JOUR);
    }


    public Jour(int annee, int mois, int jour) {
        super(new LocalDateTime(annee, mois, jour, 0, 0), DateCategorie.JOUR);
    }


    public Jour(Date date) {
        super(date, DateCategorie.JOUR);
    }

    
    public Jour(java.util.Date date) {
        super(date, DateCategorie.JOUR);
    }


    private Jour(LocalDateTime dateLocale) {
        super(dateLocale, DateCategorie.JOUR);
    }


    public Jour withYear(int year) {
        return new Jour(dateLocale.withYear(year));
    }


    public Jour plusYears(int years) {
        return new Jour(dateLocale.plusYears(years));
    }


    public Jour minusYears(int years) {
        return new Jour(dateLocale.minusYears(years));
    }


    public Jour withMonthOfYear(int month) {
        return new Jour(dateLocale.withMonthOfYear(month));
    }


    public Jour plusMonths(int months) {
        return new Jour(dateLocale.plusMonths(months));
    }


    public Jour minusMonths(int months) {
        return new Jour(dateLocale.minusMonths(months));
    }


    public Jour withDayOfMonth(int day) {
        return new Jour(dateLocale.withDayOfMonth(day));
    }


    public Jour plusDays(int days) {
        return new Jour(dateLocale.plusDays(days));
    }


    public Jour minusDays(int days) {
        return new Jour(dateLocale.minusDays(days));
    }


    public static int daysBetween(Jour debut, Jour fin) {
        return Date.between(debut, fin, DateCategorie.JOUR);
    }

    public Jour plus(int days) {
        return plusDays(days);
    }
    
    
    /**
     * Transforme une chaîne de caractère en une instance de l'objet Jour.
     * On suppose que le jour est au format harmonica (aaaammjj).
     * Si la chaîne ne représente pas une valeur conforme, la valeur nulle est retournée.
     */
    public static Jour stringToJour(String unJour) {
        Date dateResultat = Date.stringToDate(unJour, DateCategorie.JOUR); 
        return dateResultat != null ? new Jour(dateResultat) : null;
    }
    
    /**
     * Tranformer une chaîne en instance de l'objet Date.
     * La méthode teste le format donné en paramètre.
     * Si le format convient, une date est créée.
     * Si la chaîne ne représente pas une valeur conforme, la valeur nulle est retournée.
     */
    public static Jour stringToJour(String unJour, Pattern pattern) {
       return stringToJour(unJour, pattern.getMotif());
    }

    /**
     * Tranformer une chaîne en instance de l'objet Date.
     * La méthode teste le format donné en paramètre.
     * Si le format convient, une date est créée.
     * Si la chaîne ne représente pas une valeur conforme, la valeur nulle est retournée.
     */
    public static Jour stringToJour(String unJour, String pattern) {
        Date dateResultat = Date.stringToDate(unJour, DateCategorie.JOUR, pattern);
        return dateResultat != null ? new Jour(dateResultat) : null;
    }
    
    
    public static Jour valueOf(java.util.Date dateJavaUtil){
        return dateJavaUtil != null ? new Jour(dateJavaUtil) : null;
    }
    
    public static Jour valueOf(Date date){
        return date != null ? new Jour(date) : null;
    }
    
    public static Jour valueOf(String dateString){
        return dateString != null ? new Jour(dateString) : null;
    }
    
    public static java.util.Date jourToDate(Jour jour){
    	String jourAuFormatString = jour.toString(DateCategorie.JOUR);
    	java.util.Date date = new java.util.Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		try {
			date = sdf.parse(jourAuFormatString);
			
		} catch (ParseException e) {
			e.printStackTrace();
			System.out.println("Pbl de conversion de Jour en Java.Util.Date");
		}
		return date;
    	
    }	
}
