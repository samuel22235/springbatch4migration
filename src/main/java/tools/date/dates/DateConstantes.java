package tools.date.dates;


import org.joda.time.DateTimeConstants;


public interface DateConstantes {
	
	/**
	 * Format par défaut des dates jour avec heure
	 */
	public final static String FORMAT_DEFAUT_JOUR_AVEC_HEURE = "yyyyMMdd'T'HH:mm:ss.SSS";

	/**
	 * Format par défaut des dates jour
	 */
	public final static String FORMAT_DEFAUT_JOUR = "yyyyMMdd";
	
	/**
	 * Format par défaut des dates mois
	 */
	public final static String FORMAT_DEFAUT_MOIS = "yyyyMM";
	
	/**
	 * Format par défaut des dates annees
	 */
	public final static String FORMAT_DEFAUT_ANNEE = "yyyy";

	
	
	/**
	 * Format date sans heure standard pour postgresql
	 */
	public final static String FORMAT_DATE_SANS_HEURE_POSTGRESQL = "yyyy-MM-dd";

	/**
	 * Format date avec heure standard pour postgresql
	 */
	public final static String FORMAT_DATE_AVEC_HEURE_POSTGRESQL = "yyyy-MM-dd HH:mm:ss";

    /**
     * Constante représentant le samedi dans une semaine.
     * 
     * @see Date.getDayOfWeek()
     */
    public final static int SAMEDI    = DateTimeConstants.SATURDAY;

    /**
     * Constante représentant le dimanche dans une semaine.
     * 
     * @see Date.getDayOfWeek()
     */
    public final static int DIMANCHE  = DateTimeConstants.SUNDAY;

    /**
     * Constante représentant le lundi dans une semaine.
     * 
     * @see Date.getDayOfWeek()
     */
    public final static int LUNDI     = DateTimeConstants.MONDAY;

    /**
     * Constante représentant le mardi dans une semaine.
     * 
     * @see Date.getDayOfWeek()
     */
    public final static int MARDI     = DateTimeConstants.TUESDAY;

    /**
     * Constante représentant le mercredi dans une semaine.
     * 
     * @see Date.getDayOfWeek()
     */
    public final static int MERCREDI  = DateTimeConstants.WEDNESDAY;

    /**
     * Constante représentant le jeudi dans une semaine.
     * 
     * @see Date.getDayOfWeek()
     */
    public final static int JEUDI     = DateTimeConstants.THURSDAY;

    /**
     * Constante représentant le vendredi dans une semaine.
     * 
     * @see Date.getDayOfWeek()
     */
    public final static int VENDREDI  = DateTimeConstants.FRIDAY;


    public final static int[] JOURS_DE_LA_SEMAINE = {LUNDI, MARDI, MERCREDI, JEUDI, VENDREDI, SAMEDI, DIMANCHE};
    
    public final static int[] JOURS_OUVRES_DE_LA_SEMAINE = {LUNDI, MARDI, MERCREDI, JEUDI, VENDREDI};
    
    //-------------------------------------------------------------------------


    /**
     * Constante représentant le mois de janvier dans une année.
     * 
     * @see Date.getMonthOfYear()
     */
    public final static int JANVIER   = DateTimeConstants.JANUARY;

    /**
     * Constante représentant le mois de février dans une année.
     * 
     * @see Date.getMonthOfYear()
     */
    public final static int FEVRIER   = DateTimeConstants.FEBRUARY;

    /**
     * Constante représentant le mois de mars dans une année.
     * 
     * @see Date.getMonthOfYear()
     */
    public final static int MARS      = DateTimeConstants.MARCH;

    /**
     * Constante représentant le mois d'avril dans une année.
     * 
     * @see Date.getMonthOfYear()
     */
    public final static int AVRIL     = DateTimeConstants.APRIL;

    /**
     * Constante représentant le mois de mai dans une année.
     * 
     * @see Date.getMonthOfYear()
     */
    public final static int MAI       = DateTimeConstants.MAY;

    /**
     * Constante représentant le mois de juin dans une année.
     * 
     * @see Date.getMonthOfYear()
     */
    public final static int JUIN      = DateTimeConstants.JUNE;

    /**
     * Constante représentant le mois de juillet dans une année.
     * 
     * @see Date.getMonthOfYear()
     */
    public final static int JUILLET   = DateTimeConstants.JULY;

    /**
     * Constante représentant le mois d'août dans une année.
     * 
     * @see Date.getMonthOfYear()
     */
    public final static int AOUT      = DateTimeConstants.AUGUST;

    /**
     * Constante représentant le mois de septembre dans une année.
     * 
     * @see Date.getMonthOfYear()
     */
    public final static int SEPTEMBRE = DateTimeConstants.SEPTEMBER;

    /**
     * Constante représentant le mois d'octobre dans une année.
     * 
     * @see Date.getMonthOfYear()
     */
    public final static int OCTOBRE   = DateTimeConstants.OCTOBER;

    /**
     * Constante représentant le mois de novembre dans une année.
     * 
     * @see Date.getMonthOfYear()
     */
    public final static int NOVEMBRE  = DateTimeConstants.NOVEMBER;

    /**
     * Constante représentant le mois de décembre dans une année.
     * 
     * @see Date.getMonthOfYear()
     */
    public final static int DECEMBRE  = DateTimeConstants.DECEMBER;


    public final static Integer[] MOIS_DE_L_ANNEE = {JANVIER, FEVRIER, MARS, AVRIL, MAI, JUIN, JUILLET, AOUT, SEPTEMBRE, OCTOBRE, NOVEMBRE, DECEMBRE};
    

}
