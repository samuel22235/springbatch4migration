package tools.date.dates;


import java.util.Locale;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public enum DateCategorie {

    JOUR_AVEC_HEURE (DateConstantes.FORMAT_DEFAUT_JOUR_AVEC_HEURE),
    JOUR            (DateConstantes.FORMAT_DEFAUT_JOUR),
    MOIS            (DateConstantes.FORMAT_DEFAUT_MOIS),
    ANNEE           (DateConstantes.FORMAT_DEFAUT_ANNEE);
    
    
    public static final DateCategorie DEFAUT = DateCategorie.JOUR; 

    // Voici les champs privés définis par le constructeur
    private String                    pattern;
    private final DateTimeFormatter   dateTimeFormatter;


    // Voici le constructeur invoqué pour chaque valeur ci-dessus.
    DateCategorie(String pattern) {
        this.pattern = pattern;
        this.dateTimeFormatter = DateTimeFormat.forPattern(pattern).withLocale(Locale.FRANCE);
        
    }


    // Voici les méthodes d'accès aux champs. Il s'agit de méthodes d'instance
    // pour chaque valeur du type énuméré.
    public String getPattern() {
        return pattern;
    }


    public DateTimeFormatter getDateTimeFormatter() {
        return dateTimeFormatter;
    }

}
