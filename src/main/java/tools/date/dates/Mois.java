package tools.date.dates;


import java.util.Collection;
import java.util.HashSet;

import org.joda.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown=true)
public class Mois extends DateSansHeure
{
    
    public Mois() {
        super(new LocalDateTime(), DateCategorie.MOIS);
    }


    public Mois(String mois) {
    	super(mois, DateCategorie.MOIS);
    }


    public Mois(Date date) {
        super(date, DateCategorie.MOIS);
    }


    public Mois(java.util.Date date) {
        super(date, DateCategorie.MOIS);
    }


    private Mois(LocalDateTime dateLocale) {
        super(dateLocale, DateCategorie.MOIS);
    }


    public Mois withYear(int year) {
        return new Mois(dateLocale.withYear(year));
    }


    public Mois plusYears(int years) {
        return new Mois(dateLocale.plusYears(years));
    }


    public Mois minusYears(int years) {
        return new Mois(dateLocale.minusYears(years));
    }


    public Mois withMonthOfYear(int month) {
        return new Mois(dateLocale.withMonthOfYear(month));
    }


    public Mois plusMonths(int months) {
        return new Mois(dateLocale.plusMonths(months));
    }


    public Mois minusMonths(int months) {
        return new Mois(dateLocale.minusMonths(months));
    }


    // Récupérer le mois suivant la date au format chaîne en paramètre 
    public static Mois getMoisSuivant(String date) {
        return getMoisSuivant(Date.stringToDate(date));
    }

    // Récupérer le mois suivant la date au format Date
    public static Mois getMoisSuivant(Date dateExacte) {
        Mois moisSuivant = null;
        if (dateExacte != null) {
        	if (!DateCategorie.MOIS.equals(dateExacte.categorie)) {
        		moisSuivant = new Mois(dateExacte).plusMonths(1);
        	} else {
        		moisSuivant = ((Mois) dateExacte).plusMonths(1);
        	}
        }
        return moisSuivant;
    }


    public static int monthsBetween(Mois debut, Mois fin) {
        return Date.between(debut, fin, DateCategorie.MOIS);
    }

    public Mois plus(int months) {
        return plusMonths(months);
    }
        
    
    /**
     * Transforme une chaîne de caractère en une instance de l'objet Mois.
     * On suppose que le mois est au bon format (cf. DateCategorie et fichier properties des formats associés).
     * Si la chaîne ne représente pas une valeur conforme, la valeur nulle est retournée.
     */
    public static Mois stringToMois(String unMois) {
        Date dateResultat = Date.stringToDate(unMois, DateCategorie.MOIS); 
        return dateResultat != null ? new Mois(dateResultat) : null;
    }
    
    /**
     * Tranformer une chaîne en instance de l'objet Date.
     * La méthode teste le format donné en paramètre.
     * Si le format convient, une date est créée.
     * Si la chaîne ne représente pas une valeur conforme, la valeur nulle est retournée.
     */
    public static Mois stringToMois(String unMois, Pattern pattern) {
       return stringToMois(unMois, pattern.getMotif());
    }

    /**
     * Tranformer une chaîne en instance de l'objet Date.
     * La méthode teste le format donné en paramètre.
     * Si le format convient, une date est créée.
     * Si la chaîne ne représente pas une valeur conforme, la valeur nulle est retournée.
     */
    public static Mois stringToMois(String unMois, String pattern) {
        Date dateResultat = Date.stringToDate(unMois, DateCategorie.MOIS, pattern);
        return dateResultat != null ? new Mois(dateResultat) : null;
    }
    /**
     * Permet de transformer un jour en mois.
     * @return Renvoit le premier jour du mois
     */
    
    public Jour getPremierJourDuMois(){
    	Jour jour = new Jour(this.toStringFromPattern("yyyyMM")+"01");
    	return jour;
    }
    
 
    public static Mois valueOf(java.util.Date dateJavaUtil){
        return dateJavaUtil != null ? new Mois(dateJavaUtil) : null;
    }
    
    public static Collection<Mois> valueOf(Collection<? extends Date> dates) {
    	Collection<Mois> resultat = new HashSet<Mois>();
    	for (Date date : dates) {
    		resultat.add(valueOf(date));
		}
    	return resultat;
    }
    
    public static Mois valueOf(Date date){
        return date != null ? new Mois(date) : null;
    }
    
    public static Mois valueOf(String dateString){
        return dateString != null ? new Mois(dateString) : null;
    }
    
}
